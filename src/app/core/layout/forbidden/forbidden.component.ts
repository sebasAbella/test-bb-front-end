//
// Copyright (C) 2020
// developer = Sebastian Abella Rocha
//
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forbidden',
  templateUrl: './forbidden.component.html',
  // styleUrls: ['./forbidden.component.scss']
})
export class ForbiddenComponent implements OnInit {
  constructor() {}

  public ngOnInit() {
    localStorage.clear();
    sessionStorage.clear();
  }
}
