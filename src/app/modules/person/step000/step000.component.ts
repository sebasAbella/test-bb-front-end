import { Component, OnInit, ViewChild } from '@angular/core';
import { Step001Component } from '../step001/step001.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { PersonService } from 'src/app/core/service/person/person.service';
import { SendInformationService } from 'src/app/core/service/SendInformation/send-information.service';
import { UPDATEDATA } from 'src/app/core/config/constants';

@Component({
  selector: 'app-step000',
  templateUrl: './step000.component.html',
  styleUrls: ['./step000.component.css']
})
export class Step000Component implements OnInit {
  // definir columnas
  displayedColumns: string[] = ['id', 'title', 'Edit', 'Delete'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  moment: any = moment;

  constructor(
    public dialog: MatDialog,
    private personService: PersonService,
    public obser: SendInformationService<any>
  ) {
    // observable encargado de actualizar la tabla
    this.obser.getData(UPDATEDATA).subscribe((data) => {
      if (data) {
        this.getDataPerson();
      }
    });
  }
  /**
   * funcion ecargada de asignar el filtro
   * @param filterValue valor por el que se filtra en el buscador de la tabla
   */
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * funcion para cargar la data cuando la vista este creada
   */
  ngOnInit() {
    this.getDataPerson();
  }

  /**
   * funcion encargada de traer la data de personas
   */
  getDataPerson() {
    this.personService.getPerson().subscribe(
      (response: Array<[]>) => {
        this.dataSource = new MatTableDataSource(response);
      }
      , (err) => {
        this.dataSource = new MatTableDataSource([]);
      }
    );
    this.dataSource.paginator = this.paginator;
  }

  /**
   * metodo para abrir modal validando en ella si es un editar o crear
   * @param state envia estado al dialogo para editar o actualizar
   * @param arrayEdit se envia la información al dialogo necesaria para crear o actualizar
   */
  openDialog(state: any, arrayEdit: any) {
    this.dialog.open(Step001Component, {
      height: '350px',
      width: '400px',
      data: {
        state,
        arrayEdit
      }
    });
  }

  /**
   * metodo para consumir servicio de eliminación
   * @param userKey id de la persona para remover
   */
  deleteUser(userKey: any) {
    this.personService.removePerson(userKey).subscribe((response) => {
      this.obser.sendData(true, UPDATEDATA);
    }, (err) => {
      console.log(err);
    });
  }

}
