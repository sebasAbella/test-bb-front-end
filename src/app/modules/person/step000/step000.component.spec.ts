import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step000Component } from './step000.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Step001Component } from '../step001/step001.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';


describe('Step000Component', () => {
  let component: Step000Component;
  let fixture: ComponentFixture<Step000Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTableModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatPaginatorModule,
        MatIconModule,
        ReactiveFormsModule,
        MatDialogModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      declarations: [Step000Component, Step001Component],
      providers: [MatDialog,
        Step001Component]
    })
      .overrideModule(BrowserDynamicTestingModule, { set: { entryComponents: [Step001Component] } })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step000Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test for applyFilter', () => {
    component.applyFilter('keyword');
    expect(component).toBeTruthy();
  });

  it('test for openDialog', () => {
    const arrayEdit = {
      arrayEdit: { id: 1, fullname: 'test', birth: '1999/08/08' }
    };
    component.openDialog('edit', arrayEdit);
    expect(component).toBeTruthy();
  });

  it('test for openDialog', () => {
    component.deleteUser('52');
    expect(component).toBeTruthy();
  });
});
