

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Step000Component } from './step000/step000.component';
import { Step001Component } from './step001/step001.component';

const routes: Routes = [
  {
    path: 'step000', component: Step000Component,
    canActivate: []
  },
  {
    path: 'step001', component: Step001Component,
    canActivate: []
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonRoutingModule { }


