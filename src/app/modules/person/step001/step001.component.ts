import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PersonService } from 'src/app/core/service/person/person.service';
import * as moment from 'moment';
import { SendInformationService } from 'src/app/core/service/SendInformation/send-information.service';
import { UPDATEDATA } from 'src/app/core/config/constants';

export interface DialogData {
  state: 'create' | 'edit';
  arrayEdit: { fullname: any, birth: any, completed: any, id: any, key: any };
}
@Component({
  selector: 'app-step001',
  templateUrl: './step001.component.html',
  styleUrls: ['./step001.component.css']
})
export class Step001Component {
  formRegister: FormGroup;
  nameTitle: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public formBuilder: FormBuilder,
    private personService: PersonService,
    public obser: SendInformationService<any>
  ) {
    // inicializar el formgroup segun los datos enviados de list component al dialogo
    if (data.state === 'create') {
      this.nameTitle = 'Crear registro';
      this.formRegister = this.formBuilder.group({
        fullname: ['', Validators.required],
        birth: ['', Validators.required],
      });
    } else {
      const birth = moment(this.data.arrayEdit.birth).format('YYYY-MM-DD');
      this.nameTitle = 'Editar registro';
      this.formRegister = this.formBuilder.group({
        fullname: [this.data.arrayEdit.fullname, Validators.required],
        birth: [birth, Validators.required],
      });
    }

  }
  /**
   * funcion encargada de crear o actualizar los datos segun el dialogo
   */
  addUser() {
    const json = this.formRegister.value;
    if (this.formRegister.status !== 'INVALID') {
      if (this.data.state === 'create') {
        this.personService.CreatePerson(json).subscribe(
          (response) => {
            this.obser.sendData(true, UPDATEDATA);
          },
          (err) => {
            console.log('err', err);
          });
      } else {
        json.id = this.data.arrayEdit.id;
        this.personService.updatePerson(String(json.id), json).subscribe(
          (response) => {
            this.obser.sendData(true, UPDATEDATA);
          },
          (err) => {
            console.log('err actualización', err);
          });
      }
    }
  }

}
